
interface IResponse {
  statusCode: number;
  body: string | number | boolean | {[header: string]: string | number | boolean | object};
}


const createResponse = ({
  code = 200,
  body = {},
}): IResponse => {
  return {
    statusCode: code,
    body: JSON.stringify(body),
  };
};

export const getEvent = async function() {
  return createResponse({
    code: 200,
    body: {
      response: "Welcome to the nodejs-aws-lambda API",
      method: ["GET"],
      body: "This is the a successful response",
    },
  });
};
