import { getEvent } from "../index"


describe("EVENT TESTS", () => {
it('status code equals 200', async () => {
    const eventObj = await getEvent();
    expect(eventObj.statusCode).toEqual(200);
  });


it('response should have body property', async () => {
    const eventObj = await getEvent();

    expect(eventObj).toHaveProperty('body');
  });
});