# NODEJS SERVERLESS APPLICATION HOSTED ON AWS LAMBDA

---

### Endpoint URL

This application is hoted at [aws url](https://b8g6zyc1be.execute-api.us-east-2.amazonaws.com/production/event)

### Setting Up AWS

1. Create AWS credentials with appropriate IAM policy:
2. Set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables in the GitLab CI/CD settings. `Settings > CI/CD > Variables`.

### Development

Install dependencies with:

```sh
npm install
```

```sh
npm run build
```

```sh
npm start
```

### Tests

```sh
npm run test
```
